package com.example.rockpaperscissors.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rockpaperscissors.R
import com.example.rockpaperscissors.data.Player
import com.squareup.picasso.Picasso

class PlayersAdapter(val data: MutableList<Player>): RecyclerView.Adapter<PlayersAdapter.PlayersViewHolder>() {

    class PlayersViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private var playerTextView:TextView
        private var playerImageView:ImageView
        private var currentPlayer:Player?

        init {
            playerTextView = itemView.findViewById(R.id.player_info)
            playerImageView = itemView.findViewById(R.id.player_photo)
            currentPlayer = null
        }

        fun bind(player: Player){
            this.currentPlayer = player
            this.playerTextView.text = "${player.firstName} ${player.lastName}"
            Picasso.get().load(player.photoUrl).resize(50,50).centerCrop().into(playerImageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersViewHolder {
            //za da prikazeme slika od internet url, treba da iskoristime biblioteka Picasso koja cesto se koristi
            //implementation 'com.squareup.picasso:picasso:2.8'
            Picasso.get().load(player.photoUrl).resize(50,50).centerCrop().into(playerImageView)
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayersAdapter.PlayersViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_player, parent, false)
        return PlayersViewHolder(view)
    }

    override fun onBindViewHolder(holder: PlayersAdapter.PlayersViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}
