package com.example.rockpaperscissors.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rockpaperscissors.R

//pravime ednostavna lista od stringovi
class ExampleViewAdapter(private val data: MutableList<String>) :
    RecyclerView.Adapter<ExampleViewAdapter.ViewHolder>() { //ovde mi fali viewHolder klsata
    //taa ke ja vgnezdime vo adapterot
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val textView: TextView
        private var currentString: String? = null
//za da gi zastitam ovie kegi stavam kako private
        init {
            textView = view.findViewById(R.id.txtOutput)
            textView.text = currentString
        }

        fun bind(currentString: String){
            this.currentString = currentString
            this.textView.text = currentString
        }
    }

    // za da gi popolnam metodite moram prvo da imam nekakov layout za stavkata, inicijaliziraj vo viewholder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_layout, parent, false)
        //parent mu e RecyclerView
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        //gi prebrojuva elementite vo bazata t.e. vo listata od stringovi
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //povrzuvanje na views so soodvetniot podatok
        //parametarot holder e najcesto reciklirano
//        holder.currentString = data[position]
//        holder.textView.text = holder.currentString
        holder.bind(data[position])
        //ubavo e bindingot da go pravime kako posebna funkcija vo viewholder
    }
}