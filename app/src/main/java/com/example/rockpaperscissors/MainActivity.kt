package com.example.rockpaperscissors

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.CalendarContract
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import com.example.rockpaperscissors.viewmodels.RockPaperScissorsViewModel
import java.util.Calendar

class MainActivity : AppCompatActivity() {
    private lateinit var textViewTitle: TextView
    private lateinit var editTextChoice: EditText
    private lateinit var btnSubmit: Button
    private lateinit var textViewComputerChoice: TextView
    private lateinit var textViewResult: TextView

    //CAS 2
    private lateinit var btnGoToExplicitActivity: Button
    private lateinit var btnGoToImplicitActivity: Button
    private lateinit var btnGoToIntentActivity: Button

    //CAS 3
    private lateinit var btnGoToListView: Button
    private lateinit var btnGoToPlayersActivity: Button

    //rezultatot koj treba da go dobieme
    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data //go prezemame intentot
                textViewTitle.text =
                    data?.getStringExtra("userChoice") //go citame podatokot od intentot
//za da mozeme da vratime rezultat od intent activity vo main activity treba da go trgneme startActivity podole
                //i da go zamenime so resultLauncher.launch(intent)
            }
        }

    private lateinit var viewModel: RockPaperScissorsViewModel

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if(result.resultCode == Activity.RESULT_OK){
            val data: Intent? = result.data
            textViewResult.text = data?.getStringExtra("userChoice")
        }
    }

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textViewTitle = findViewById(R.id.textViewTitle)
        editTextChoice = findViewById(R.id.editTextChoice)
        btnSubmit = findViewById(R.id.btnSubmit)
        textViewComputerChoice = findViewById(R.id.textViewComputerChoice)
        textViewResult = findViewById(R.id.textViewResult)
        //CAS 2
        btnGoToExplicitActivity = findViewById<Button>(R.id.btnGoToExplicitActivity)
        btnGoToImplicitActivity = findViewById<Button>(R.id.btnGoToImplicitActivity)
        btnGoToIntentActivity = findViewById<Button>(R.id.btnGoToIntentActivity)

        //CAS 3
        btnGoToListView = findViewById<Button>(R.id.btnGoToListView)
        btnGoToPlayersActivity = findViewById<Button>(R.id.btnGoToPlayersActivity)

        viewModel = ViewModelProvider(this)[RockPaperScissorsViewModel::class.java]

//        btnSubmit.setOnClickListener {
//            viewModel.setUserChoice(editTextChoice.text.toString())
//            val computerChoice: String = viewModel.submitChoice()
//            textViewComputerChoice.text = computerChoice
//            textViewResult.text = viewModel.winner(computerChoice)
//        }

        //CAS 2
        btnGoToExplicitActivity.setOnClickListener {
            //val intent: Intent = Intent(this, ExplicitActivity::class.java)
            Intent(this, ExplicitActivity::class.java).let { i ->
                i.putExtra(
                    "userChoice",
                    editTextChoice.text.toString()
                ) //ovde go prakjame podatokot i trea soodvetno da go iscitame od explicitActivity
                startActivity(i)
            }
//            intent.putExtra("userChoice", editTextChoice.text.toString()) //ovde go prakjame podatokot i trea soodvetno da go iscitame od explicitActivity
//            startActivity(intent)
            //posto se poslednive 2 reda tesno povrzani so intentot mozeme da go ogranicime scopeot na intentot

        }

        //ke gi probame site primeri od predavanjata
        btnGoToImplicitActivity.setOnClickListener {
            //1. otvaranje google maps so specificirana lokacija
            // startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/?q=Rugjer Boskovikj Skopje"))) //ke otvori soodvetna mapa so lokacijata Rugjer Boskovikj Skopje
            //treba da se setira i permission za internet vo manifestot
            //2. prakjanje email
//            val intent = Intent(Intent.ACTION_SEND).let { emailIntent ->
//                emailIntent.setType("text/plain")
//                emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("jon@example.com"))
//                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject")
//                emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text")
//                startActivity(emailIntent)
//            }

            //3. dodavanje event vo kalendar
//            Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI).let { cal ->
//                val beginTime: Calendar = Calendar.getInstance()
//                beginTime.set(2023, 11, 8, 7, 30)
//                val endTime: Calendar = Calendar.getInstance()
//                endTime.set(2023, 11, 8, 10, 30)
//
//                cal.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.timeInMillis)
//                cal.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.timeInMillis)
//
//                cal.putExtra(CalendarContract.Events.TITLE, "Learn Kotlin")
//                cal.putExtra(CalendarContract.Events.EVENT_LOCATION, "Online")
//
//                startActivity(cal)
//            }

            //4. otvaranje druga web strana vo browser
            val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://developer.android.com"))
            //uri = nique sequence of characters that identifies a logical or physical resource used by web technologies
            startActivity(webIntent)

            //imate opcija i vasa app da se dodade vo lista na apps sto poddrzuvaat odredena akcija,
            //da se odi od edna app na druga
        }

//        _________________________________________________________________________________

        btnGoToIntentActivity.setOnClickListener { _ -> //ova _ e za ako ne ni treba view-to
            Intent().apply { //mozeme site promeni da gi stavime vo apply za da gi primenime vednas
//koga kreirame implicit intent potrebna ni e akcijata i formatot na podatoci
                action =
                    "mk.ukim.finki.rockpaperscissors" //sega od strana na main activity sakame da go povikame intent activity
                //a toa IntentActivity gi slusa site intenti od celiot OS
                type = "text/plain"
            }.let { i ->
                i.putExtra(
                    "userChoice",
                    editTextChoice.text.toString()
                ) //ovde go prakjame podatokot i trea soodvetno da go iscitame
                //startActivity(Intent.createChooser(i, "Choose the app for your intent")) //i explicit i implicit intenti se povikuvaat so startActivity
                //so ova chooser proveruvame dali postoi app koja moze da go opsluzi intentot

                resultLauncher.launch(i) //mu kazuvame na main activity deka nazad ke dobie nekoj rezultat
            }
        }

        // CAS 3 _________________________________________________________________________________
        btnGoToListView.setOnClickListener {
            startActivity(Intent(this, ListViewActivity::class.java))
        }

        btnGoToPlayersActivity.setOnClickListener {
            startActivity(Intent(this, PlayersListActivity::class.java))
        }





        editTextChoice.addTextChangedListener { newText ->
            viewModel.setUserChoice(newText.toString())
            textViewComputerChoice.text = viewModel.submitChoice()
            textViewResult.text = viewModel.winner(textViewComputerChoice.text.toString())
        }

        viewModel.getUserChoiceValue().observe(this){
            textViewComputerChoice.text = viewModel.submitChoice()
            textViewResult.text = viewModel.winner(textViewComputerChoice.text.toString())
        }
    }

//    fun submitChoice():String{
//        val result = (1..3).random()
//        val computerChoice = when(result){
//            1 -> "Rock"
//            2 -> "Paper"
//            else -> "Scissors"
//        }
//        return computerChoice
//    }

//    fun winner(userChoice: String, computerChoice: String):String{
//        val winner = when{
//            userChoice == "Rock" && computerChoice == "Scissors" -> "You win!"
//            userChoice == "Paper" && computerChoice == "Rock" -> "You win!"
//            userChoice == "Scissors" && computerChoice == "Paper" -> "You win!"
//            userChoice == computerChoice -> "It's a tie!"
//            else -> "You lose!"
//        }
//        return winner
//    }
}