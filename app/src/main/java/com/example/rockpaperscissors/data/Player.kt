package com.example.rockpaperscissors.data

class Player(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val photoUrl: String,
)