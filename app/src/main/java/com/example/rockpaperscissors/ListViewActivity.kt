package com.example.rockpaperscissors

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.rockpaperscissors.adapter.ExampleViewAdapter

class ListViewActivity : AppCompatActivity() {

    private lateinit var listView: RecyclerView

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_view)

        listView = findViewById(R.id.listView)

        listView.adapter = ExampleViewAdapter(loadData())
    }

    private fun loadData(): MutableList<String>{
        return mutableListOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")
    }
}