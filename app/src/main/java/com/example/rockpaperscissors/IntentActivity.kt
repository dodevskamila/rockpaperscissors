package com.example.rockpaperscissors

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.example.rockpaperscissors.viewmodels.RockPaperScissorsViewModel

class IntentActivity : AppCompatActivity() {
    //sega ke ja iscitame vrednosta od bundle koja sme ja pratile od MainActivity
    private lateinit var txtUserChoice: TextView
    private lateinit var btnSubmit: Button
    private lateinit var viewModel: RockPaperScissorsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        txtUserChoice = findViewById(R.id.txtUserChoice)
        btnSubmit = findViewById(R.id.btnSubmit)

        //od samiot intent da izvleceme bundle i od nego da ja procitame promenlivata
        viewModel = ViewModelProvider(this)[RockPaperScissorsViewModel::class.java]

        var bundle: Bundle? = intent.extras
        viewModel.setUserChoice(bundle?.getString("userChoice").toString())
        txtUserChoice.text = bundle?.getString("userChoice")

        btnSubmit.setOnClickListener {
            Intent().let { intent ->
                intent.putExtra("userChoice", "TODO")
                setResult(RESULT_OK, intent) //go vrakja nazad rezultatot
                finish()
            }
        }
    }
}