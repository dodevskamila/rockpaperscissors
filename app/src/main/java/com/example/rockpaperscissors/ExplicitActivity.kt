package com.example.rockpaperscissors

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.rockpaperscissors.viewmodels.RockPaperScissorsViewModel

class ExplicitActivity : AppCompatActivity() {
    private lateinit var txtUserChoice: TextView
    //by se koristi za vednas inicijalizacija bez da stavam vo onCreate
    private lateinit var viewModel: RockPaperScissorsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explicit)

        txtUserChoice = findViewById(R.id.txtUserChoice)
    //ispratenite extras od intentot se zacuvuvaat vo bundle

        viewModel = ViewModelProvider(this)[RockPaperScissorsViewModel::class.java]

        viewModel.getUserChoiceValue().observe(this){
            //txtUserChoice.text = viewModel.getUserChoiceValue().value
            txtUserChoice.text = viewModel.submitChoice()
        } //subscribe na promenite na userChoice vo viewModel

        var bundle: Bundle? = intent.extras
        val choice: String? = bundle?.getString("userChoice") ?: "None" //mozeme da go pristapime podatokot od bundle so getString
        viewModel.setUserChoice(choice.toString() ?: "None") //ke ja dodelime vrednosta na userChoice vo viewModel
        //ubavo e vreddnosta vednas da ja iscitame i da napravime subscribe na promenite (pogore pred bunlde)

        //tuka imame setValue, no vo liveData postoi i postValue metod,
        // razlikata e vo toa sto setValue e thread safe, a postValue ne e thread safe
        //t.e. setValue se smeta deka pravi promeni na Main Thread,
        // a postValue se smeta deka pravi promeni na nekoja Background Thread
        //za ova povekje koga ke rabotime so Kotlin Coroutines
        //sega smetame deka podatocite patuvaat od Main Thread do Main Thread povtorno
        txtUserChoice.text = choice //ke ja dodelime vrednosta na ova textview
    }
}