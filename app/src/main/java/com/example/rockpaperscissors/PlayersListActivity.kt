package com.example.rockpaperscissors

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.example.rockpaperscissors.adapter.PlayersAdapter
import com.example.rockpaperscissors.data.playersList
import com.squareup.picasso.Picasso

class PlayersListActivity : AppCompatActivity() {

    private lateinit var listView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_players_list)

        listView = findViewById(R.id.listView)

        listView.adapter = PlayersAdapter(playersList())
    }
}