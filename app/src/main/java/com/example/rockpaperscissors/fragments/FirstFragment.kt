package com.example.rockpaperscissors.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.commit
import com.example.rockpaperscissors.R
import com.example.rockpaperscissors.databinding.FragmentFirstBinding
import com.example.rockpaperscissors.viewmodels.UsernameViewModel

class FirstFragment : Fragment(R.layout.fragment_first) {
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
//    private lateinit var btnSecond: Button

    private val usernameViewModel: UsernameViewModel by activityViewModels()

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
////        usernameViewModel = ViewModelProvider(this)[UsernameViewModel::class.java]
//
//    }
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        btnSecond = view.findViewById(R.id.btnGoToSecond)
        _binding = FragmentFirstBinding.bind(view)


        binding.btnGoToSecond.setOnClickListener {
            usernameViewModel.setUsername(binding.editUsername.text.toString())
            parentFragmentManager.commit {
                replace(R.id.fragment_container_view, SecondFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
            }
        }

        usernameViewModel.username.observe(viewLifecycleOwner){
            binding.textView.text = it
        }

    }

}