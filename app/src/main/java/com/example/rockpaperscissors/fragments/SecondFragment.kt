package com.example.rockpaperscissors.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.rockpaperscissors.R
import com.example.rockpaperscissors.databinding.FragmentSecondBinding
import com.example.rockpaperscissors.viewmodels.UsernameViewModel

class SecondFragment : Fragment(R.layout.fragment_second) {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!
    private val usernameViewModel: UsernameViewModel by activityViewModels()

//      override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
//        return inflater.inflate(, container, false)
//    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSecondBinding.bind(view)

        usernameViewModel.username.observe(viewLifecycleOwner){
            binding.txtUsername.text = it
        }

        binding.txtUsername.text = usernameViewModel.username.value
    }

}