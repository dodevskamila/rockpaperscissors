package com.example.rockpaperscissors

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import com.example.rockpaperscissors.fragments.FirstFragment

class FragmentExampleActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_example)

        if(savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.fragment_container_view, FirstFragment())
                setReorderingAllowed(true)
//                addToBackStack(null)
            }
        }
    }
}