package com.example.rockpaperscissors.models

class Player (
    val id: Long,
    val firstName: String,
    val lastName: String,
    val photoUrl: String,
)