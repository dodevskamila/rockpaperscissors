package com.example.rockpaperscissors.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UsernameViewModel: ViewModel() {
    private val mutableUsername = MutableLiveData<String>()
    val username: LiveData<String> get() = mutableUsername


    fun setUsername(username: String){
        mutableUsername.value = username
    }

}